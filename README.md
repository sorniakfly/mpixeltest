# README #

This is a simple todoApp for wordpress. It is a plugin, you will need to use shortcode in order to make it work.

### Set up? ###

* Just create folder _mpixeltest_ into your _/wp-content/plugins/_, then activate plugin in dashboard. 
* You can also download the _release_ archive from download section (file _mpixeltest.zip_ )
* Paste shorcode [ mpixeltest_form ] to any post\page, after publishing\previewing it will render small box with of todoApp interface
* You may provide optional parameter to shortcode, like this [ mpixeltest_form width=500 ], where _500_ can be any number, just keep in mind that this number defines maximum width for rendered _mpixeltest_ todo application in pixels.

### How to use? ###
* Typing in input field, then pressing "Enter" will write\modify your task to database. By default all new tasks are added unfinished (i.e. checkboxes not checked)
* By clicking the checkbox near your task you will mark it as done and  it also will be automatically written to database
* If you will press "Enter" while leaving task input field empty - that will remove the task from the list and database. So, remove text of task, press "Enter" and no task to do, and no worries...!

### TODO ###
* dynamic load of js scripts, so it would appear only on pages with shortcode
* add removal of todo apps database on deactivation pluging 
* ...
