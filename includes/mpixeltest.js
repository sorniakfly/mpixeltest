window.addEventListener( "load", function () {
    document.querySelectorAll('form.mpixeltest-form__control > div input[name="task"]').forEach(item => {
        item.addEventListener('keydown', event => {helperEventTextInput(event);} )
    })
    document.querySelectorAll('form.mpixeltest-form__control > div input[name="status"]').forEach(item => {
        item.addEventListener('click', event => {
            event.target.parentNode.className = 'mpixeltest-current-wrapper';//'marked-for-update';
            helperSendData();
        } )
    })

    const form = document.getElementsByClassName( "mpixeltest-form__control" )[0];

    form.addEventListener( "submit", function ( event ) {
      event.preventDefault();
    });
});

function helperIsInteger(value) {
    return /^\d+$/.test(value);
}

function helperEventTextInput (event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      event.target.parentNode.className = 'mpixeltest-current-wrapper';
      helperSendData()

      if ((event.target.className == 'latter') && (event.target.value == '')) { // Delete
        event.target.parentNode.parentNode.removeChild(event.target.parentNode);
      }
      else if ((event.target.className == 'first') && (event.target.value !== '')) { // Insert
          let div = document.createElement('div');
          div.addEventListener('keydown', event => {helperEventTextInput (event);} );
          div.className = 'mpixeltest-form__input-wrapper';
          div.innerHTML = `<input type="checkbox" name="status" value="" class="marked-for-update"><input type="text" name="task" value="${event.target.value}" class="latter">`;
          event.target.parentNode.append(div);
          event.target.value = '';
      }

      console.log(event.target.className);
      console.log(event.target);
    }
}

function helperSendData() {
    const request = new XMLHttpRequest();

    request.open('POST', MPIXELTESTJS.ajaxUrl, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            console.log(this.response, "200 <-> 400");
            if (helperIsInteger(this.response)) {
                marked_for_update_element = document.getElementsByClassName("marked-for-update")[0]
                marked_for_update_element.value = this.response;
                marked_for_update_element.className = 'latter';
                document.getElementsByClassName("mpixeltest-form__status")[0].innerHTML = "<div class='mpixeltest-form__status--success'><p>New task Successfully submitted!</p></div>";
                helperHideNote(document.getElementsByClassName("mpixeltest-form__status")[0], 2000);
            } 
            else if (this.response=="success") {
                document.getElementsByClassName("mpixeltest-form__status")[0].innerHTML = "<div class='mpixeltest-form__status--success'><p>Task was deleted!</p></div>";
                helperHideNote(document.getElementsByClassName("mpixeltest-form__status")[0], 2000);
            } 
            else if (this.response=="false") {
                document.getElementsByClassName("mpixeltest-form__status")[0].innerHTML = "<div class='mpixeltest-form__status--failure'><p>Empty task is not allowed!</p></div>";
                helperHideNote(document.getElementsByClassName("mpixeltest-form__status")[0], 2000);
            } else {
                document.getElementsByClassName("mpixeltest-form__status")[0].innerHTML = "<div class='mpixeltest-form__status--success'><p>Task was updated!</p></div>";
                helperHideNote(document.getElementsByClassName("mpixeltest-form__status")[0], 2000);
            } 
        } else {
            console.log(this.response, "Check the connection...");
        }
    };
    request.send('action=make_mpixeltest_call&task=' 
        + document.querySelector('div.mpixeltest-current-wrapper input[name="task"]').value 
        + '&status=' + document.querySelector('div.mpixeltest-current-wrapper input[name="status"]').checked
        + '&id=' + document.querySelector('div.mpixeltest-current-wrapper input[name="status"]').value);
    document.querySelector('.mpixeltest-current-wrapper').className = 'mpixeltest-form__input-wrapper';
}

function helperHideNote(htmlObject, milisecs) {
    setTimeout(()=>{
        htmlObject.innerHTML = ""; 
    },milisecs)
}