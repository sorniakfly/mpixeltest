<?php
class mainMPixelTest {
    public function __construct() {
        add_action('wp_ajax_nopriv_make_mpixeltest_call', array($this, 'ajax_output'));
        add_action('wp_ajax_make_mpixeltest_call', array($this, 'ajax_output'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_styles_and_scripts'));
        add_shortcode('mpixeltest_form', array($this, 'shortcode_with_form_code')); //[mpixeltest_form]
    }
    function enqueue_styles_and_scripts() {
        wp_enqueue_script('mpixeltest_js', plugin_dir_url(__FILE__) . 'mpixeltest.min.js');
        wp_enqueue_style('mpixeltest_css',  plugin_dir_url(__FILE__) . 'mpixeltest.min.css');
        wp_add_inline_script( 'mpixeltest_js', 'const MPIXELTESTJS = ' . json_encode( array(
            'ajaxUrl' => admin_url( 'admin-ajax.php' ),
            'keyParam' => 'passKey',
        )), 'before' );
    }

    function ajax_output() {
        global $wpdb;
        $task = $_POST['task'];
        $status = $_POST['status'];
        $id = $_POST['id'];
            if ($task=='' && $id=='') {           // Empty insert - error
                echo "false";
            }
            else if ($task=='' && $id!=='') {     // Delete
                $wpdb->delete(
                    $wpdb->prefix . "mpixeltest",
                    ['id' => $_POST['id']]
                );
                echo "success";
            }  
            else if ($task!=='' && $id!=='') {          // Update
                $checked = $_POST['status'] == 'true' ? ' checked ' : '';
                echo '<div class="mpixeltest-form__input-wrapper"><input type="checkbox" value="' . $_POST['id'] . '" name="status"' . $checked . 'value=""><input type="text" name="task" value="' . $_POST['task']. '"></div>';
                $wpdb->update(
                    $wpdb->prefix . "mpixeltest",
                    [ 'todo_task' => $_POST['task'], 'todo_status' => $_POST['status']], 
                    ['id' => $_POST['id']]
                );
            } else {                                    // Insert
                $wpdb->insert($wpdb->prefix . "mpixeltest", array(
                    "todo_task" => $task,
                    "todo_status" => $status
                ));
                $lastid = $wpdb->insert_id;
                echo $lastid;
            }
        die();
    }

    function shortcode_with_form_code($atts = array(), $content = null) {
        $parameter_width = shortcode_atts( array(
            'width' => '213'
         ), $atts );
        return $this->show_form($parameter_width['width']);
    }

    function show_form($width_parameter) {
        global $wpdb;

        $table_name = $wpdb->prefix . "mpixeltest";
        $records=$wpdb->get_results('SELECT * FROM ' . $table_name . ' ;');
        $all = '';
        $checked = '';

        foreach($records as $key => $row): 
            $checked = $row->todo_status == 'true' ? ' checked ' : '';
            $all.='<div class="mpixeltest-form__input-wrapper"><input type="checkbox" value="' . $row->id . '" name="status"' . $checked . 'value=""><input class="latter" type="text" name="task" value="' . $row->todo_task . '"></div>';
        endforeach;

        return '<section class="mpixeltest-form" style="width:' . $width_parameter . 'px;"><div class="mpixeltest-form__status"></div>
        <form action="" class="mpixeltest-form__control">
            <div class="mpixeltest-form__input-wrapper">
                <input type="checkbox" name="status" value="" class="first" disabled>
                <input type="text" name="task" value="" class="first" placeholder="Enter new tasks here...">
            </div>'
            .$all.
            '<input type="submit" value="" >
        </form></section>';
    }

} 