<?php
/*
Plugin Name: Sorniaks mPixelTest PLugin
Plugin URI: 
Description: Simple ToDo List
Version: 0.1
Author: DSH Sorniak
Author URI: 
License: GPLv2
*/

define('MPIXELTEST_FILE', __FILE__);
define('MPIXELTEST_PATH', plugin_dir_path(__FILE__));

require MPIXELTEST_PATH.'includes/mainMPixelTest.php';

function create_table() {
    global $wpdb;

    $table_name = $wpdb->prefix . "mpixeltest";
    $charset_collate = $wpdb->get_charset_collate();
    if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) 
    {
        $sql = 'CREATE TABLE '. $table_name .' ( ';
        $sql .= "  id  int(11)   NOT NULL AUTO_INCREMENT, ";
        $sql .= "  todo_task varchar(255), ";
        $sql .= "  todo_status BOOL, UNIQUE KEY id (id)";
        $sql .= ")" . $charset_collate;
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }
}

register_activation_hook( __FILE__, 'create_table' );

new mainMPixelTest();
